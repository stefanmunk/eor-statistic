module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'Europa Orient Rallye Ronnies Statistik',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Europa Orient Rallye Ronnies Statistik der Cahllanges, Videos, usw' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
     ** Customize the progress bar color
     */
    loading: { color: '#3B8070' },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** Run ESLint on save
         */
        extend(config, ctx) {
            if (ctx.dev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    generate: {
        routes: [
            '/statistiken'
        ]
    },
    plugins: ['~/plugins/components.js'],
    modules: [
        ['storyblok-nuxt', { accessToken: '3osYq64IJl5bhALsIsje5wtt', cacheProvider: 'memory' }]
    ]
}