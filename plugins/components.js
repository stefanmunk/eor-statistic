import Vue from 'vue'
import Page from '@/components/Page.vue'
import Teaser from '@/components/Teaser.vue'
import Grid from '@/components/Grid.vue'
import Feature from '@/components/Feature.vue'
import event from '@/components/event.vue'
import eventGroup from '@/components/eventGroup.vue'
import eventGroupPage from '@/components/eventGroupPage.vue'

Vue.component('page', Page)
Vue.component('teaser', Teaser)
Vue.component('grid', Grid)
Vue.component('feature', Feature)
Vue.component('event', event)
Vue.component('eventgroup', eventGroup)
Vue.component('eventGroupPage', eventGroupPage)
